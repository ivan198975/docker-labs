#!/bin/bash
################################
#  Depois que subir container  #
################################

/etc/init.d/cron start
/etc/init.d/rundeckd start
systemctl enable rundeckd 

echo "*/1 * * * * root bash /tmp/script-startup.sh" /etc/crontab

/etc/init.d/cron restart 
