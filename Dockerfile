FROM debian:8.5
LABEL maintainer="ivan_ornaghi@yahoo.com.br"
COPY ./script-startup.sh /tmp/

RUN apt-get update -y
RUN apt-get install -y wget 
RUN apt-get install -y vim
RUN apt-get install -y cron
RUN apt-get install -y curl
RUN apt-get install -y openssh-client
RUN apt-get install openjdk-7-jdk -y
RUN dpkg --add-architecture i386
RUN wget http://dl.bintray.com/rundeck/rundeck-deb/rundeck-2.6.7-1-GA.deb
RUN dpkg -i ./rundeck-2.6.7-1-GA.deb

RUN ./tmp/script-startup.sh

EXPOSE 4440



